<?php


namespace App\Services;


class Math
{

    /**
     * Get a random float between two int according to the number of digit
     *
     * @param int $min
     * @param int $max
     * @param int $decimals
     * @return float
     */
    public static function getRandomFloat(int $min, int $max, int $decimals = 2) {
        $divisor = 10*$decimals ;
        return mt_rand($min*$divisor, $max * $divisor) / $divisor;
    }
}
