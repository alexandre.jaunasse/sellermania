<?php


namespace App\Tests\Controller;


use Symfony\Bundle\FrameworkBundle\Test\WebTestCase;
use Symfony\Component\HttpFoundation\Response;

class SecurityControllerTest extends WebTestCase
{
    /**
     * @dataProvider getUrl
     * @param string $httpMethod
     * @param string $url
     */
    public function testPageOk(string $httpMethod, string $url) {
        $client = static::createClient();
        $client->request($httpMethod, $url);
        $this->assertSame(Response::HTTP_OK, $client->getResponse()->getStatusCode());
    }

    public function getUrl()
    {
        yield ['GET', '/login'];
    }

}
