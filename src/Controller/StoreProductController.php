<?php


namespace App\Controller;


use App\Entity\ProductStore;
use App\Entity\Store;
use Doctrine\ORM\EntityManagerInterface;
use Symfony\Bundle\FrameworkBundle\Controller\AbstractController;
use Symfony\Component\HttpFoundation\Request;
use Symfony\Component\Routing\Annotation\Route;

/**
 * Controller use to manage product of the store of the user
 *
 * @Route("/store/product", name="store_product_")
 * Class StoreController
 * @package App\Controller
 */
class StoreProductController extends AbstractController
{

    /**
     * List of all product in the current store
     *
     * @Route("/", name="index")
     * @param Request $request
     * @return \Symfony\Component\HttpFoundation\Response
     */
    public function index(Request $request, EntityManagerInterface $manager) {
        $storeProducts = $manager->getRepository(ProductStore::class)->findByStore($this->getUser()->getStore()) ;

        return $this->render('produtStore/index.html.twig', [
            'storeProducts' => $storeProducts
        ]);
    }

}
