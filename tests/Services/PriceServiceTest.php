<?php


namespace App\Tests\Services;


use App\Repository\ProductMarketplaceRepository;
use App\Services\PriceService;
use Doctrine\Common\Persistence\ObjectManager;
use Doctrine\ORM\EntityManagerInterface;
use PHPUnit\Framework\TestCase;

class PriceServiceTest extends TestCase
{

    /**
     * @dataProvider provideValues
     * @param $minPrice
     * @param $maxPrice
     * @param $level
     * @param $expected
     */
    public function testGenerateSuggestedPrice($minPrice, $maxPrice, $level, $expected) {
        // Mock the repository
        $productMarketplaceRepository = $this->createMock(ProductMarketplaceRepository::class);

        //Mock the first call of the query in the function
        $productMarketplaceRepository->expects($this->at(0))
            ->method('findConcurrentLowPriceBySate')
            ->willReturn($minPrice) ;

        //Mock the second call of the query in the function
        $productMarketplaceRepository->expects($this->at(1))
            ->method('findConcurrentLowPriceBySate')
            ->willReturn($maxPrice) ;

        //Mock the object maanger
        $objectManager = $this->createMock(ObjectManager::class);
        $objectManager->expects($this->any())
            ->method('getRepository')
            ->willReturn($productMarketplaceRepository);

        //Instance of the service
        $priceService = new PriceService($objectManager) ;

        //Call the function to test
        $price = $priceService->generateSuggestedPrice(1,1,20, $level, 1) ;

        //Assert the value
        $this->assertEquals($expected, $price) ;
    }

    public function provideValues()
    {
        return [
            [9, 15, 8, 8.99],
            [25, 24, 20, 23],
            [10, 15, 11, 11],
            [null, 25.45, 20, 24.45],
            [23.22, null, 20, 23.21],
            [null, null, 20, 20],
            [19, null, 20, 20],
            [null, 18, 20, 20],
            [20, 23, 20, 20],
            [24, 20, 20, 20],
            [null, 20, 20, 20],
            [20, null, 20, 20],
            [20, 20, 20, 20],
        ];
    }
}
