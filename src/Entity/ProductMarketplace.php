<?php

namespace App\Entity;

use Doctrine\Common\Collections\ArrayCollection;
use Doctrine\Common\Collections\Collection;
use Doctrine\ORM\Mapping as ORM;

/**
 * @ORM\Entity(repositoryClass="App\Repository\ProductMarketplaceRepository")
 * @codeCoverageIgnore
 */
class ProductMarketplace
{
    /**
     * @ORM\Id()
     * @ORM\GeneratedValue()
     * @ORM\Column(type="integer")
     */
    private $id;

    /**
     * @ORM\ManyToOne(targetEntity="App\Entity\Marketplace", inversedBy="productMarketplaces")
     */
    private $marketplace;

    /**
     * @ORM\Column(type="float")
     */
    private $price;

    /**
     * @ORM\ManyToOne(targetEntity="App\Entity\ProductStore", inversedBy="productMarketplaces")
     */
    private $productStore;


    public function __construct()
    {
        $this->storeProducts = new ArrayCollection();
    }

    public function getId(): ?int
    {
        return $this->id;
    }

    public function getMarketplace(): ?Marketplace
    {
        return $this->marketplace;
    }

    public function setMarketplace(?Marketplace $marketplace): self
    {
        $this->marketplace = $marketplace;

        return $this;
    }


    public function getPrice(): ?float
    {
        return $this->price;
    }

    public function setPrice(float $price): self
    {
        $this->price = $price;

        return $this;
    }

    public function getProductStore(): ?ProductStore
    {
        return $this->productStore;
    }

    public function setProductStore(?ProductStore $productStore): self
    {
        $this->productStore = $productStore;

        return $this;
    }


}
