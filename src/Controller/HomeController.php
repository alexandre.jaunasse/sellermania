<?php


namespace App\Controller;


use App\Entity\Marketplace;
use Doctrine\ORM\EntityManagerInterface;
use Symfony\Bundle\FrameworkBundle\Controller\AbstractController;
use Symfony\Component\Routing\Annotation\Route;


class HomeController extends AbstractController
{

    /**
     * @Route("/", name="homepage")
     */
    public function index(EntityManagerInterface $manager) {

        $amazonMarketplace = $manager->getRepository(Marketplace::class)->findOneByLibelle(Marketplace::LIB_AMAZON) ;
        return $this->render('index.html.twig', [
                'amazonMarketplace' => $amazonMarketplace
            ]);
    }

}
