<?php

namespace App\DataFixtures;

use App\Entity\Category;
use App\Entity\Marketplace;
use App\Entity\Product;
use App\Entity\ProductMarketplace;
use App\Entity\ProductStore;
use App\Entity\StateProduct;
use App\Entity\Store;
use App\Entity\User;
use App\Services\PriceService;
use App\Services\StateService;
use Doctrine\Bundle\FixturesBundle\Fixture;
use Doctrine\Common\Persistence\ObjectManager;
use Symfony\Component\Security\Core\Encoder\UserPasswordEncoderInterface;

class AppFixtures extends Fixture
{

    /** @var UserPasswordEncoderInterface $passwordEncoder */
    private $passwordEncoder;
    private $priceService ;
    private $stateService ;

    public function __construct(UserPasswordEncoderInterface $passwordEncoder, PriceService $priceService, StateService $stateService)
    {
        $this->passwordEncoder = $passwordEncoder;
        $this->priceService = $priceService;
        $this->stateService = $stateService;
    }


    public function load(ObjectManager $manager)
    {
        $this->loadCategory($manager) ;
        $this->loadStateProduct($manager) ;
        $this->loadMarketplace($manager) ;
        $this->loadStore($manager) ;
        $this->loadUsers($manager);
        $this->loadProducts($manager) ;
        $this->loadProductsStoreConcurrent($manager) ;
        $this->loadProductMarketplaceConcurrent($manager) ;
        $this->loadProductCurrentStore($manager) ;
        // $product = new Product();
        // $manager->persist($product);

        $manager->flush();
    }

    private function loadCategory(ObjectManager $manager)
    {
        $category = new Category();
        $category->setLibelle(Category::LIBELLE_VIDEO_GAME) ;
        $manager->persist($category) ;
        $manager->flush();
    }

    private function loadStateProduct(ObjectManager $manager)
    {
        foreach (StateProduct::LIST_STATE as $key => $value) {
            $stateProduct = new StateProduct() ;
            $stateProduct->setLibelle($value) ;
            $stateProduct->setCode($key) ;
            $manager->persist($stateProduct) ;
        }

        $manager->flush() ;
    }


    private function loadMarketplace(ObjectManager $manager)
    {
        $marketplace = new Marketplace();
        $marketplace->setLibelle(Marketplace::LIB_AMAZON) ;
        $manager->persist($marketplace) ;
        $manager->flush() ;
    }


    private function loadProducts(ObjectManager $manager)
    {
        $category = $manager->getRepository(Category::class)->findOneByLibelle(Category::LIBELLE_VIDEO_GAME) ;
        foreach ($this->getVideoGameData() as [$name, $description]) {
            $product = new Product() ;
            $product->setName($name)
                ->setDescription($description)
                ->setCategory($category);

            $manager->persist($product) ;
        }
        $manager->flush();
    }

    private function loadStore(ObjectManager $manager)
    {
        foreach ($this->getStoresData() as [$name, $address, $zip, $city, $country]) {
            $store = new Store() ;
            $store->setName($name)
                ->setAddress($address)
                ->setCity($city)
                ->setZipCode($zip)
                ->setCountry($country) ;

            $manager->persist($store) ;
        }

        $manager->flush() ;
    }

    private function loadUsers(ObjectManager $manager)
    {
        $store = $manager->getRepository(Store::class)->findOneByName('AlexGame') ;
        $user = new User() ;
        $user->setEmail('admin@mail.fr')
            ->setFullName('Alexandre Jaunasse')
            ->setUsername('admin')
            ->setPassword($this->passwordEncoder->encodePassword($user, 'admin'))
            ->setRoles(['ROLE_ADMIN'])
            ->setStore($store);

        $manager->persist($user) ;
        $manager->flush();
    }


    private function loadProductsStoreConcurrent(ObjectManager $manager)
    {
        /** @var Store $currentStore */
        $currentStore = $manager->getRepository(Store::class)->findOneByName('AlexGame') ;
        $storesConcurrent = $manager->getRepository(Store::class)->findConcurrentStore($currentStore->getId()) ;

        $products = $manager->getRepository(Product::class)->findAll() ;
        foreach ($products as $product) {
            foreach ($storesConcurrent as $store) {
                $productStore = new ProductStore() ;
                $productStore
                    ->setStore($store)
                    ->setState($this->stateService->getRandomState())
                    ->setProduct($product) ;
                $manager->persist($productStore) ;
            }
        }
        $manager->flush();
    }

    private function loadProductMarketplaceConcurrent(ObjectManager $manager)
    {
        /** @var Store $currentStore */
        $currentStore = $manager->getRepository(Store::class)->findOneByName('AlexGame') ;
        $marketplace = $manager->getRepository(Marketplace::class)->findOneByLibelle(Marketplace::LIB_AMAZON) ;
        $productStoreConcurrent = $manager->getRepository(ProductStore::class)->findAllConcurent($currentStore->getId()) ;

        /** @var ProductStore $productStoreConcurrent */
        foreach ($productStoreConcurrent as $productStoreConcurrent) {
            $marketplaceProduct = new ProductMarketplace() ;
            $marketplaceProduct->setMarketplace($marketplace)
                ->setPrice($this->priceService->setPriceAccordingState($productStoreConcurrent->getState()->getLibelle()))
                ->setProductStore($productStoreConcurrent) ;
            $manager->persist($marketplaceProduct) ;
        }

        $manager->flush() ;

    }

    private function loadProductCurrentStore(ObjectManager $manager)
    {
        /** @var Store $currentStore */
        $currentStore = $manager->getRepository(Store::class)->findOneByName('AlexGame') ;
        $products = $manager->getRepository(Product::class)->findAll() ;
        foreach ($products as $product) {
            /** @var StateProduct $stateProduct */
            $stateProduct = $this->stateService->getRandomState() ;
            $productStore = new ProductStore();
            $productStore->setProduct($product)
                ->setState($stateProduct)
                ->setStore($currentStore)
                ->setLevel($this->priceService->getLevelAccordingState($stateProduct->getLibelle())) ;
            $manager->persist($productStore) ;
        }
        $manager->flush() ;
    }


    private function getStoresData(): array
    {
        return [
            // $storesData = [$name, $address, $zip, $city, $country];
            ['AlexGame', 'rue de nantes', '44000', 'Nantes', 'France'],
            ['Abc jeux', 'rue de paris', '67000', 'Strasbourg', 'France'],
            ['Games-planete', 'rue de strasbourg', '75000', 'Paris', 'France'],
            ['Media-games', 'rue de marseille', '13000', 'Marseille', 'France'],
            ['Top-Jeux-video', 'rue de lille', '59000', 'Lille', 'France'],
            ['Tous-les-jeux', 'rue de bordeaux', '33000', 'Bordeaux', 'France'],
            ['Diffusion-133', 'rue de cholet', '49300', 'Cholet', 'France'],
            ['France-video', 'rue de rennes', '35000', 'Rennes', 'France'],
        ];
    }

    private function getVideoGameData(): array  {
        return [
            // $product = [$name, $description]
            ['GTA V', 'Grand Theft Auto V est un jeu vidéo d\'action-aventure, développé par Rockstar North et édité par Rockstar Games.'],
            ['Fifa 19', 'Jeux de foot développé par FIfa'],
            ['Call of duty', 'Jeux d\'action développé par Ubisoft'],
            ['Far cry III', 'Jeux d\'aventure'],
        ] ;
    }




}
