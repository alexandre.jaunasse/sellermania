<?php

namespace App\Repository;

use App\Entity\ProductMarketplace;
use Doctrine\Bundle\DoctrineBundle\Repository\ServiceEntityRepository;
use Doctrine\ORM\AbstractQuery;
use Symfony\Bridge\Doctrine\RegistryInterface;

/**
 * @method ProductMarketplace|null find($id, $lockMode = null, $lockVersion = null)
 * @method ProductMarketplace|null findOneBy(array $criteria, array $orderBy = null)
 * @method ProductMarketplace[]    findAll()
 * @method ProductMarketplace[]    findBy(array $criteria, array $orderBy = null, $limit = null, $offset = null)
 */
class ProductMarketplaceRepository extends ServiceEntityRepository
{
    public function __construct(RegistryInterface $registry)
    {
        parent::__construct($registry, ProductMarketplace::class);
    }


    /**
     * Query witch retrieve all concurrent by the 'best' offer
     *
     * @return mixed
     */
    public function getConcurrentByBestOffer() {
        return $this->createQueryBuilder('pm')
            ->join('pm.productStore', 'ps')
            ->join('ps.product', 'p')
            ->join('ps.store', 's')
            ->join('ps.state', 'st')
            ->select('pm, ps, p, s')
            ->orderBy('st.id', 'DESC')
            ->addOrderBy('pm.price', 'ASC')
            ->getQuery()
            ->getResult()
            ;
    }

    public function findConcurrentLowPriceBySate($idMarketplace, $idProduct, $codeState, $idCurrentStore) {
          $result = $this->createQueryBuilder('pm')
            ->join('pm.productStore', 'ps')
            ->join('pm.marketplace', 'm')
            ->join('ps.product', 'p')
            ->join('ps.store', 's')
            ->join('ps.state', 'st')
            ->select('pm.price')
            ->where('s.id <> :idCurrentStore')
            ->andWhere('m.id = :idMarketplace')
            ->andWhere('st.code = :codeState')
            ->andWhere('p.id = :productId')
            ->setParameters([
                'idCurrentStore'  => $idCurrentStore,
                'idMarketplace'   => $idMarketplace,
                'codeState'       => $codeState,
                'productId'       => $idProduct
            ])
            ->orderBy('pm.price', 'ASC')
            ->getQuery()
            ->getResult()
            ;
          if(!$result) {
              return null ;
          }
          return $result[0]['price'] ;
    }
    // /**
    //  * @return ProductMarketplace[] Returns an array of ProductMarketplace objects
    //  */
    /*
    public function findByExampleField($value)
    {
        return $this->createQueryBuilder('p')
            ->andWhere('p.exampleField = :val')
            ->setParameter('val', $value)
            ->orderBy('p.id', 'ASC')
            ->setMaxResults(10)
            ->getQuery()
            ->getResult()
        ;
    }
    */

    /*
    public function findOneBySomeField($value): ?ProductMarketplace
    {
        return $this->createQueryBuilder('p')
            ->andWhere('p.exampleField = :val')
            ->setParameter('val', $value)
            ->getQuery()
            ->getOneOrNullResult()
        ;
    }
    */
}
