<?php

namespace App\Entity;

use Doctrine\ORM\Mapping as ORM;

/**
 * @ORM\Entity(repositoryClass="App\Repository\StateProductRepository")
 * @codeCoverageIgnore
 */
class StateProduct
{
    const LIB_ETAT_MOYEN    = 'Etat moyen' ;
    const LIB_ETAT_BON      = 'Bon état' ;
    const LIB_ETAT_TB       = 'Très bon état' ;
    const LIB_ETAT_CM       = 'Comme neuf' ;
    const LIB_ETAT_NEUF     = 'Neuf' ;

    const LIST_STATE = [
        10 => self::LIB_ETAT_MOYEN,
        20 => self::LIB_ETAT_BON,
        30 => self::LIB_ETAT_TB,
        40 => self::LIB_ETAT_CM,
        50 => self::LIB_ETAT_NEUF,
    ] ;
    /**
     * @ORM\Id()
     * @ORM\GeneratedValue()
     * @ORM\Column(type="integer")
     */
    private $id;

    /**
     * @ORM\Column(type="string", length=25)
     */
    private $libelle;

    /**
     * @ORM\Column(type="integer")
     */
    private $code;

    public function getId(): ?int
    {
        return $this->id;
    }

    public function getLibelle(): ?string
    {
        return $this->libelle;
    }

    public function setLibelle(string $libelle): self
    {
        $this->libelle = $libelle;

        return $this;
    }

    public function getCode(): ?int
    {
        return $this->code;
    }

    public function setCode(int $code): self
    {
        $this->code = $code;

        return $this;
    }
}
