<?php


namespace App\Tests\Controller;


use App\Entity\Marketplace;
use Symfony\Bundle\FrameworkBundle\Test\WebTestCase;
use Symfony\Component\HttpFoundation\Response;

class MarketplaceControllerTest extends WebTestCase
{

    /**
     * @dataProvider getUrl
     * @param string $httpMethod
     * @param string $url
     */
    public function testPageOk(string $httpMethod, string $url) {
        $client = static::createClient([], [
            'PHP_AUTH_USER' => 'admin@mail.fr',
            'PHP_AUTH_PW' => 'admin'
        ]);

        $crawler = $client->request($httpMethod, $url);


        $this->assertSame(Response::HTTP_OK, $client->getResponse()->getStatusCode());
        $this->assertEquals('marketplace_product_concurrent', $client->getRequest()->attributes->get('_route')) ;
        $this->assertGreaterThanOrEqual(
            1,
            $crawler->filter('body .row  #main .table ')->count(),
            'La page s\'affiche bien'
        );
    }

    public function getUrl()
    {
        yield ['GET', '/marketplace/'  . Marketplace::LIB_AMAZON . '/product/concurrent'];
    }

}
