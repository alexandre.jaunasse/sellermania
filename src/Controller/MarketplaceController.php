<?php


namespace App\Controller;


use App\Entity\Marketplace;
use App\Entity\ProductMarketplace;
use App\Entity\ProductStore;
use App\Entity\Store;
use App\Services\PriceService;
use Doctrine\ORM\EntityManagerInterface;
use Sensio\Bundle\FrameworkExtraBundle\Configuration\ParamConverter;
use Symfony\Bundle\FrameworkBundle\Controller\AbstractController;
use Symfony\Component\HttpFoundation\Request;
use Symfony\Component\HttpFoundation\Response;
use Symfony\Component\Routing\Annotation\Route;

/**
 * Class who manage marketplace's actions
 *
 * @Route("/marketplace", name="marketplace_")
 * Class MarketplaceController
 * @package App\Controller
 */
class MarketplaceController extends AbstractController
{
    /**
     * @Route("/", name="index")
     * @param Request $request
     * @param EntityManagerInterface $manager
     */
    public function index(Request $request, EntityManagerInterface $manager) {
    }

    /**
     * @Route("/{marketplaceName}/product/concurrent", name="product_concurrent")
     * @ParamConverter("marketplace", options={"mapping"={"marketplaceName"="libelle"}})
     * @param Request $request
     * @param EntityManagerInterface $manager
     * @return Response
     */
    public function productConcurrentByMarketplace(Request $request, Marketplace $marketplace, EntityManagerInterface $manager) {
        $currentStore = $this->getUser()->getStore() ;

        $marketplaceProducts = $manager->getRepository(ProductMarketplace::class)->getConcurrentByBestOffer($currentStore->getId()) ;

        return $this->render('marketplace/product_concurrent.html.twig', [
            'marketplaceProducts' => $marketplaceProducts
        ]) ;
    }

    /**
     * @Route("/add/{marketplaceName}/{id}", name="add_product")
     * @param Request $request
     * @param EntityManagerInterface $manager
     * @param Marketplace $marketplace
     * @param ProductStore $productStore
     * @param PriceService $priceService
     * @ParamConverter("marketplace", options={"mapping"={"marketplaceName"="libelle"}})
     * @return \Symfony\Component\HttpFoundation\RedirectResponse
     */
    public function addProductInMarketplace(Request $request, EntityManagerInterface $manager, Marketplace $marketplace, ProductStore $productStore, PriceService $priceService) {
        /**
         * Get the current store
         * @var Store $currentStore
         */
        $currentStore = $this->getUser()->getStore();

        // We generate a price according to the two concurrent price
        $suggestedPrice = $priceService->generateSuggestedPrice(
            $marketplace->getId(),
            $productStore->getProduct()->getId(),
            $productStore->getState()->getCode(),
            $productStore->getLevel(),
            $currentStore->getId()
        ) ;

        //Creating the marketplace product object
        $productMarketplace = new ProductMarketplace();
        $productMarketplace->setProductStore($productStore)
            ->setMarketplace($marketplace)
            ->setPrice($suggestedPrice) ;
        //Add in database
        $manager->persist($productMarketplace) ;
        $manager->flush() ;

        //add a flash message to prevent the user.
        $this->addFlash(
            'success',
            'Le produit ' . $productStore->getProduct()->getName(). ' a correctement ajouté sur' .
            $marketplace->getLibelle() . ' au prix de : ' .$suggestedPrice . '€'
        ) ;

        //Redirect to index product of the store
        return $this->redirectToRoute('store_product_index') ;

    }

    /**
     * @param EntityManagerInterface $manager
     * @return Response
     */
    public function sideBar(EntityManagerInterface $manager) {
        return $this->render('components/sidebar.html.twig', [
            'marketplaces' => $manager->getRepository(Marketplace::class)->findAll()
        ]) ;
    }

}
