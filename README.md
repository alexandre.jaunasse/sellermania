Requirements
------------

  * PHP 7.1.3 or higher;
  * Mysql or Mariadb
  * npm or yarn
  * Composer

Installation
------------

Install de php dependencies

```bash
$ composer install
```

Install npm dependencies

With yarn
```bash
$ yarn
```

With npm
```bash
$ npm install
```

Compile asset

```bash
$ yarn run dev
```
or
```bash
$ npm run dev
```

Database
-----
Modify the .env to specify you database environment

To create the database run :
```bash
$ php bin/console doctrine:database:create
```

Run the migration:

```bash
$ php bin/console doctrine:migration:migrate
```

Load the datafixtures

```bash
$ php bin/console doctrine:fixtures:load
```

Usage
-----
To run web server:

```bash
$ php bin/console server:run
```

By default, it your web server on  http://localhost:8001


Tests
-----

Execute this command to run tests:

```bash
$ php bin/phpunit --coverage-text
```
