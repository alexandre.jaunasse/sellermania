<?php


namespace App\Services;


use App\Entity\Product;
use App\Entity\ProductMarketplace;
use App\Entity\StateProduct;
use App\Repository\ProductMarketplaceRepository;
use Doctrine\Common\Persistence\ObjectManager;
use Doctrine\ORM\EntityManagerInterface;

class PriceService
{

    protected $manager ;

    public function __construct(ObjectManager $entityManager)
    {
        $this->manager = $entityManager ;
    }

    /**
     * Set a random price according to the state
     * This function is use for the fixtures
     *
     * Etat moyen       - Price between 9 and 15
     * Bon état         - Price between 16 and 21€
     * Très bon etat    - Price between 22 and 27€
     * Comme neuf       - Price between 28 and 33€
     * Neuf             - Price between 34 and 40€
     */
    public function setPriceAccordingState(string $libelle) {

        switch ($libelle) {
            case StateProduct::LIB_ETAT_MOYEN :
                return Math::getRandomFloat(9 ,15) ;
                break;
            case StateProduct::LIB_ETAT_BON:
                return  Math::getRandomFloat(16 ,21) ;
                break;
            case StateProduct::LIB_ETAT_TB:
                return Math::getRandomFloat(22 ,27) ;
                break;
            case StateProduct::LIB_ETAT_CM:
                return Math::getRandomFloat(28 ,33) ;
                break;
            case StateProduct::LIB_ETAT_NEUF:
                return Math::getRandomFloat(34 ,40) ;
                break;
        }

    }

    /**
     * Retrieve the level of our product for the current store
     *
     * Etat moyen       = 9€
     * Bon état         = 16€
     * Très bon état    = 22€
     * Comme neuf       = 28€
     * Neuf             = 34€
     * @param string $stateProduct
     * @return mixed
     */
    public function getLevelAccordingState(string $stateProduct)
    {
       return Product::LIST_LEVEL[$stateProduct] ;
    }

    /**
     * Generate a price for a product to be concurrent in a marketplace
     * The price is generated according to this specification :
     *
     *  - Be 0.01€ less expensive than the 'best' concurrent with the same state
     *  - Be 1€ less expensive than the 'best' concurrent with one greater state
     *  - If all the price are greater than the level of the product
     *
     * @param int $idMarketplace
     * @param int $idProduct
     * @param int $codeState
     * @param int $level
     * @param int $currentStoreId
     * @return float
     */
    public function generateSuggestedPrice(int $idMarketplace, int $idProduct, int $codeState, int $level, int $currentStoreId): float
    {
        /** @var ProductMarketplaceRepository $productMarketRepo */
        $productMarketRepo = $this->manager->getRepository(ProductMarketplace::class) ;

        /** @var float $productConcurrentWithGreaterState */
        // We get the concurrent product with low price with same state in the current marketplace
        $priceOfConcurrentWithSameState = $productMarketRepo
            ->findConcurrentLowPriceBySate($idMarketplace, $idProduct, $codeState, $currentStoreId) ;


        /** @var float $productConcurrentWithGreaterState */
        // We get the concurrent product with low price with one greater state in the current marketplace
        $priceOfConcurrentWithGreaterState =  $productMarketRepo
            ->findConcurrentLowPriceBySate($idMarketplace, $idProduct, $codeState + 10, $currentStoreId) ;


        // If the price of product with the same state is less than the product with greater state and greater than the level
        // Than we return the price 0.01€ less expensive than the price of product with the same state
        // Otherwise if the price the product with greater state and greater than the level
        // Than we return the price 1€ less expensive than the price of the product with greater state
        // Otherwise we return the level of the product's price.

        if(($priceOfConcurrentWithSameState <= $priceOfConcurrentWithGreaterState
            && $priceOfConcurrentWithSameState > $level) ||
            (!$priceOfConcurrentWithGreaterState && $priceOfConcurrentWithSameState > $level)
        ) {
            return $priceOfConcurrentWithSameState - 0.01 ;
        }
        else if(($priceOfConcurrentWithGreaterState <= $priceOfConcurrentWithSameState
            && $priceOfConcurrentWithGreaterState > $level) ||
            (!$priceOfConcurrentWithSameState && $priceOfConcurrentWithGreaterState > $level)
        ) {
            return $priceOfConcurrentWithGreaterState - 1 ;
        }
        return $level;

    }


}
