var Encore = require('@symfony/webpack-encore');

Encore
  .setOutputPath('public/build/')
  .setPublicPath('/build')
  .cleanupOutputBeforeBuild()
  .autoProvidejQuery()
  .autoProvideVariables({
  })
  .enableSassLoader()
  .enableVersioning(Encore.isProduction())
  .addEntry('app', './assets/js/app.js')
  .splitEntryChunks()
  .enableSingleRuntimeChunk()
  .enableIntegrityHashes()
  .configureBabel(null, {
    useBuiltIns: 'usage',
    corejs: 3,
  })
;

module.exports = Encore.getWebpackConfig();
