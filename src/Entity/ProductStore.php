<?php

namespace App\Entity;

use Doctrine\Common\Collections\ArrayCollection;
use Doctrine\Common\Collections\Collection;
use Doctrine\ORM\Mapping as ORM;

/**
 * @ORM\Entity(repositoryClass="App\Repository\ProductStoreRepository")
 * @codeCoverageIgnore
 */
class ProductStore
{
    /**
     * @ORM\Id()
     * @ORM\GeneratedValue()
     * @ORM\Column(type="integer")
     */
    private $id;

    /**
     * @ORM\Column(type="float", nullable=true)
     */
    private $level;

    /**
     * @ORM\ManyToOne(targetEntity="App\Entity\Product")
     * @ORM\JoinColumn(nullable=false)
     */
    private $product;

    /**
     * @ORM\ManyToOne(targetEntity="App\Entity\Store", inversedBy="productStores")
     * @ORM\JoinColumn(nullable=false)
     */
    private $store;

    /**
     * @ORM\ManyToOne(targetEntity="App\Entity\StateProduct")
     * @ORM\JoinColumn(nullable=false)
     */
    private $state;

    /**
     * @ORM\OneToMany(targetEntity="App\Entity\ProductMarketplace", mappedBy="productStore")
     */
    private $productMarketplaces;


    public function __construct()
    {
        $this->productMarketplaces = new ArrayCollection();
    }

    public function getId(): ?int
    {
        return $this->id;
    }

    public function getLevel(): ?float
    {
        return $this->level;
    }

    public function setLevel(float $level): self
    {
        $this->level = $level;

        return $this;
    }

    public function getProduct(): ?Product
    {
        return $this->product;
    }

    public function setProduct(?Product $product): self
    {
        $this->product = $product;

        return $this;
    }

    public function getStore(): ?Store
    {
        return $this->store;
    }

    public function setStore(?Store $store): self
    {
        $this->store = $store;

        return $this;
    }

    public function getState(): ?StateProduct
    {
        return $this->state;
    }

    public function setState(?StateProduct $state): self
    {
        $this->state = $state;

        return $this;
    }

    /**
     * @return Collection|ProductMarketplace[]
     */
    public function getProductMarketplaces(): Collection
    {
        return $this->productMarketplaces;
    }

    public function addProductMarketplace(ProductMarketplace $productMarketplace): self
    {
        if (!$this->productMarketplaces->contains($productMarketplace)) {
            $this->productMarketplaces[] = $productMarketplace;
            $productMarketplace->setProductStore($this);
        }

        return $this;
    }

    public function removeProductMarketplace(ProductMarketplace $productMarketplace): self
    {
        if ($this->productMarketplaces->contains($productMarketplace)) {
            $this->productMarketplaces->removeElement($productMarketplace);
            // set the owning side to null (unless already changed)
            if ($productMarketplace->getProductStore() === $this) {
                $productMarketplace->setProductStore(null);
            }
        }

        return $this;
    }



}
