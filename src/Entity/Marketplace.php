<?php

namespace App\Entity;

use Doctrine\Common\Collections\ArrayCollection;
use Doctrine\Common\Collections\Collection;
use Doctrine\ORM\Mapping as ORM;

/**
 * @ORM\Entity(repositoryClass="App\Repository\MarketPlaceRepository")
 * @codeCoverageIgnore
 */
class Marketplace
{
    const LIB_AMAZON = 'Amazon';
    /**
     * @ORM\Id()
     * @ORM\GeneratedValue()
     * @ORM\Column(type="integer")
     */
    private $id;

    /**
     * @ORM\Column(type="string", length=50)
     */
    private $libelle;


    /**
     * @ORM\OneToMany(targetEntity="App\Entity\ProductMarketplace", mappedBy="marketplace")
     */
    private $productMarketplaces;

    public function __construct()
    {
        $this->productMarketplaces = new ArrayCollection();
    }

    public function getId(): ?int
    {
        return $this->id;
    }

    public function getLibelle(): ?string
    {
        return $this->libelle;
    }

    public function setLibelle(string $libelle): self
    {
        $this->libelle = $libelle;

        return $this;
    }


    /**
     * @return Collection|ProductMarketplace[]
     */
    public function getProductMarketplaces(): Collection
    {
        return $this->productMarketplaces;
    }

    public function addProductMarketplace(ProductMarketplace $productMarketplace): self
    {
        if (!$this->productMarketplaces->contains($productMarketplace)) {
            $this->productMarketplaces[] = $productMarketplace;
            $productMarketplace->setMarketplace($this);
        }

        return $this;
    }

    public function removeProductMarketplace(ProductMarketplace $productMarketplace): self
    {
        if ($this->productMarketplaces->contains($productMarketplace)) {
            $this->productMarketplaces->removeElement($productMarketplace);
            // set the owning side to null (unless already changed)
            if ($productMarketplace->getMarketplace() === $this) {
                $productMarketplace->setMarketplace(null);
            }
        }

        return $this;
    }
}
