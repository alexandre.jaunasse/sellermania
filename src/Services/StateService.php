<?php


namespace App\Services;


use App\Entity\StateProduct;
use Doctrine\ORM\EntityManagerInterface;

class StateService
{
    protected $entityManager ;

    public function __construct(EntityManagerInterface $entityManager)
    {
        $this->entityManager = $entityManager ;
    }

    /**
     * Retrieve a random state for a product according to an index
     *
     * 1 - Etat moyen
     * 2 - Bon état
     * 3 - Très bon état
     * 4 - Comme neuf
     * 5 - Neuf
     * @return mixed
     */
    public function getRandomState() {
        $randomIndex = rand(1, 5) * 10 ;
        return $this->entityManager->getRepository(StateProduct::class)->findOneByLibelle(StateProduct::LIST_STATE[$randomIndex]) ;
    }

}
