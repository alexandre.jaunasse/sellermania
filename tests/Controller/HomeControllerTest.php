<?php


namespace App\Tests\Controller;


use App\Entity\User;
use Symfony\Bundle\FrameworkBundle\Test\WebTestCase;
use Symfony\Component\BrowserKit\Cookie;
use Symfony\Component\HttpFoundation\Response;
use Symfony\Component\Security\Core\Authentication\Token\UsernamePasswordToken;

class HomeControllerTest extends WebTestCase
{
    /**
     * @dataProvider getUrl
     * @param string $httpMethod
     * @param string $url
     */
    public function testAccessDeniedForAnonymeUser(string $httpMethod, string $url)
    {
        $client = static::createClient();
        $client->request($httpMethod, $url);
        $client->followRedirect();
        $this->assertEquals('security_login', $client->getRequest()->attributes->get('_route')) ;
    }

    /**
     * @dataProvider getUrl
     * @param string $httpMethod
     * @param string $url
     */
    public function testAccessOkForUser(string $httpMethod, string $url) {
        $client = static::createClient([], [
            'PHP_AUTH_USER' => 'admin@mail.fr',
            'PHP_AUTH_PW' => 'admin'
        ]);

        $crawler = $client->request($httpMethod, $url);


        $this->assertSame(Response::HTTP_OK, $client->getResponse()->getStatusCode());
        $this->assertEquals('homepage', $client->getRequest()->attributes->get('_route')) ;
        $this->assertGreaterThanOrEqual(
            1,
            $crawler->filter('body .row  #main .jumbotron')->count(),
            'La page s\'affiche bien.'
        );
    }

    public function getUrl()
    {
        yield ['GET', '/'];
    }

}
