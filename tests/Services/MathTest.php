<?php


namespace App\Tests\Services;

use App\Services\Math;
use PHPUnit\Framework\TestCase;

class MathTest extends TestCase
{
    /**
     * @dataProvider provideIntervals
     */
   public function testGetRandomFloat($min, $max, $digits) {
        $randomFloat = Math::getRandomFloat($min, $max, $digits) ;
        $this->assertGreaterThanOrEqual($min, $randomFloat) ;
        $this->assertLessThanOrEqual($max, $randomFloat) ;
   }

    public function provideIntervals()
    {
        return [
            [9, 15, 2],
            [20, 25, 2],
            [10, 40, 2],
            [300, 400, 2],
            [50, 60, 2],
            [10, 80, 2],
            [90, 150, 2],
        ];
    }


}
