<?php

namespace App\Repository;

use App\Entity\StateProduct;
use Doctrine\Bundle\DoctrineBundle\Repository\ServiceEntityRepository;
use Symfony\Bridge\Doctrine\RegistryInterface;

/**
 * @method StateProduct|null find($id, $lockMode = null, $lockVersion = null)
 * @method StateProduct|null findOneBy(array $criteria, array $orderBy = null)
 * @method StateProduct[]    findAll()
 * @method StateProduct[]    findBy(array $criteria, array $orderBy = null, $limit = null, $offset = null)
 */
class StateProductRepository extends ServiceEntityRepository
{
    public function __construct(RegistryInterface $registry)
    {
        parent::__construct($registry, StateProduct::class);
    }

    // /**
    //  * @return StateProduct[] Returns an array of StateProduct objects
    //  */
    /*
    public function findByExampleField($value)
    {
        return $this->createQueryBuilder('s')
            ->andWhere('s.exampleField = :val')
            ->setParameter('val', $value)
            ->orderBy('s.id', 'ASC')
            ->setMaxResults(10)
            ->getQuery()
            ->getResult()
        ;
    }
    */

    /*
    public function findOneBySomeField($value): ?StateProduct
    {
        return $this->createQueryBuilder('s')
            ->andWhere('s.exampleField = :val')
            ->setParameter('val', $value)
            ->getQuery()
            ->getOneOrNullResult()
        ;
    }
    */
}
